// Load required packages
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

// Define our client schema
var ListSchema   = new mongoose.Schema({
    list: { type: String, required: true },
    userId: {type: Schema.Types.ObjectId, ref: 'User'},
    groupId: {type: Schema.Types.ObjectId, ref: 'Group'}
},{
    timestamps: true
});

// Export the Mongoose model
module.exports = mongoose.model('List', ToDoSchema);

// Load required packages
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

// Define our client schema
var TaskSchema   = new mongoose.Schema({
    task: { type: String, required: true },
    completed: { type: Boolean, default: false },
    dueDate: { type: Date, default: Date.now },
    userId: {type: Schema.Types.ObjectId, ref: 'User'},
    groupId: {type: Schema.Types.ObjectId, ref: 'Group'},
    listId: {type: Schema.Types.ObjectId, ref: 'List'}
},{
    timestamps: true
});

// Export the Mongoose model
module.exports = mongoose.model('Task', ToDoSchema);

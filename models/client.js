/**
 * Created by phangty on 26/9/16.
 */
// Load required packages
var mongoose = require('mongoose');

// Define our client schema
var ClientSchema = new mongoose.Schema({
    name: { type: String, unique: true, required: true },
    id: { type: String, required: true },
    secret: { type: String, required: true },
    userId: {type: Schema.Types.ObjectId, ref: 'User'}
}, {
    timestamps: true
});

// Export the Mongoose model
module.exports = mongoose.model('Client', ClientSchema);

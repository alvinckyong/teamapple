// Load required packages
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

// Define our client schema
var GroupSchema   = new mongoose.Schema({
    groupName: { type: String, required: true },
    userId: {type: Schema.Types.ObjectId, ref: 'User'}
},{
    timestamps: true
});

// Export the Mongoose model
module.exports = mongoose.model('Group', ToDoSchema);

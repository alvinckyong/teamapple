var express = require('express')
var colors = require('colors')
var mongoose = require('mongoose');
var ejs = require('ejs');
var passport = require('passport');

var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var session = require('express-session');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/todo-api');

// Create our Express application
var app = express();

// Set view engine to ejs
app.set('view engine', 'ejs');

// Use the body-parser package in our application
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());

// Use express session support since OAuth2orize requires it
app.use(session({
    secret: 'Super Secret Session Key',
    saveUninitialized: true,
    resave: true
}));

// Use the passport package in our application
app.use(passport.initialize());


var router = express.Router();
var controllers = require('./controllers');
controllers.set(router, app);

app.get("/callback", function(req,res){
    res.render('callback');
});

var server = app.listen(3000, function() {
    console.log('Listening on port %d'.green, server.address().port)
});
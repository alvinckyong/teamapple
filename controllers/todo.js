// Load required packages
var Todo = require('../models/task');

// Create endpoint /api/todos for POST
exports.postTodos = function(req, res) {
    var todo = new Todo({
        todo: req.body.todo,
        completed: req.body.completed,
        dueDate: req.body.dueDate,
        category: req.body.category
    });

    todo.save(function(err) {
        if (err)
            return res.send(err);

        res.json({ message: 'New To Do created !' });
    });
};

// Create endpoint /api/users for GET
exports.getTodos = function(req, res) {
    Todo.find(function(err, users) {
        if (err)
            return res.send(err);

        res.json(users);
    });
};

exports.deleteTodos = function(req, res) {
    Todo.find(function(err, users) {
        if (err)
            return res.send(err);

        res.json(users);
    });
};